using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;


public class DrawManager : MonoBehaviour
{
    public GameObject prefab;
    Plane planeObj;
    GameObject theTrail;


    public UIStateHandler uiHandler;
    public Slider mainSlider;

    // Start is called before the first frame update
    void Start()
    {
        planeObj = new Plane(Camera.main.transform.forward * -1, this.transform.position);
    }


    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        Vector3 temp = Input.mousePosition;
        temp.z = 10f;
        this.transform.position = Camera.main.ScreenToWorldPoint(temp);

        switch (uiHandler.toolstate)
        {
            case UIStateHandler.toolState.pen:
                handleDrawing();
                break;

            case UIStateHandler.toolState.eraser:
                if (Input.GetMouseButton(0))                
                    handleErasing();                
                break;

            case UIStateHandler.toolState.delete:
                if (Input.GetMouseButton(0))
                    handleDeleting();                
                break;
        }
    }


    private void handleDrawing()
    {
        if (Input.GetMouseButtonDown(0))
        {
            theTrail = Instantiate(prefab, this.transform.position, Quaternion.identity);
            theTrail.GetComponent<TrailRenderer>().widthMultiplier = mainSlider.value;
        }
        else if (Input.GetMouseButton(0))
        {
            Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float _dis;
            if (planeObj.Raycast(mouseray, out _dis))
            {
                //Debug.Log($"{_dis}, {mouseray.GetPoint(_dis)}");
                theTrail.transform.position = mouseray.GetPoint(_dis);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Mesh mesh = new Mesh();
            theTrail.GetComponent<TrailRenderer>().BakeMesh(mesh);

            theTrail.GetComponent<MeshFilter>().mesh = mesh;
            theTrail.GetComponent<MeshRenderer>().material = theTrail.GetComponent<TrailRenderer>().material;
            theTrail.GetComponent<MeshCollider>().sharedMesh = theTrail.GetComponent<MeshFilter>().mesh;

            Destroy(theTrail.GetComponent<TrailRenderer>());
            theTrail.transform.Translate(-theTrail.transform.position);
        }
    }

    private void handleDeleting()
    {
        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            Destroy(hit.transform.gameObject);          
        }
    }

    private void handleErasing()
    {
        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if ((meshCollider == null || meshCollider.sharedMesh == null) && (hit.triangleIndex * 3 + 2 <= meshCollider.sharedMesh.triangles.Length))
                return;

            meshCollider.sharedMesh = DeleteTriangle(hit.triangleIndex, meshCollider.sharedMesh);
            hit.transform.GetComponent<MeshFilter>().mesh = meshCollider.sharedMesh;
        }
    }

    private Mesh DeleteTriangle(int triangleIndex, Mesh currentMesh)
    {
        var triangles = currentMesh.triangles.ToList();

        triangles.RemoveAt(triangleIndex * 3);
        triangles.RemoveAt(triangleIndex * 3);
        triangles.RemoveAt(triangleIndex * 3);
        currentMesh.triangles = triangles.ToArray();
        currentMesh.RecalculateNormals();
        return currentMesh;
    }

}
