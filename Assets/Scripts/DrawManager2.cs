
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class DrawManager2 : MonoBehaviour
{

    public GameObject prefab;
    public Material material;
    Plane planeObj;
    GameObject instantiated_line;
    List<Vector3> points=new List<Vector3>();
    List<GameObject> lines = new List<GameObject>();

    public UIStateHandler uiHandler;
    public Slider mainSlider;

    // Start is called before the first frame update
    void Start()
    {
        planeObj = new Plane(Camera.main.transform.forward * -1, this.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        Vector3 temp = Input.mousePosition;
        temp.z = 10f;
        this.transform.position = Camera.main.ScreenToWorldPoint(temp);

        switch (uiHandler.toolstate)
        {
            case UIStateHandler.toolState.pen:
                handleDrawing();
                break;

            case UIStateHandler.toolState.eraser:
                if (Input.GetMouseButton(0))
                    handleErasing();
                break;

            case UIStateHandler.toolState.delete:
                if (Input.GetMouseButton(0))
                    handleDeleting();
                break;
        }

    }

    private Vector3 mousePos = Vector3.zero;
    private void handleDrawing()
    {
        if (Input.GetMouseButtonDown(0))
        {            
            mousePos = Input.mousePosition;
            instantiated_line = (GameObject)Instantiate(prefab, this.transform.position, Quaternion.identity);
            instantiated_line.name = "line " + lines.Count().ToString();
            instantiated_line.GetComponent<LineRenderer>().widthMultiplier = mainSlider.value;
        }
        else if (Input.GetMouseButton(0)&& (Input.mousePosition-mousePos)!=Vector3.zero)
        {
            Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float _dis;
            if (planeObj.Raycast(mouseray, out _dis))
            {
                points.Add(mouseray.GetPoint(_dis));
                var renderer = instantiated_line.GetComponent<LineRenderer>();
                renderer.positionCount += 1;
                renderer.SetPositions(points.ToArray());
            }
            mousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            points.Clear();
            Mesh mesh = new Mesh();
            instantiated_line.GetComponent<LineRenderer>().BakeMesh(mesh);
            Destroy(instantiated_line.GetComponent<LineRenderer>());
            
            instantiated_line.GetComponent<MeshFilter>().mesh = mesh;
            instantiated_line.GetComponent<MeshCollider>().sharedMesh = mesh;
            instantiated_line.GetComponent<MeshRenderer>().material = material;
            instantiated_line.transform.Translate(-instantiated_line.transform.position);

            lines.Add(instantiated_line);
        }
    }

    private void handleDeleting()
    {
        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            Destroy(hit.transform.gameObject);
        }
    }

    private void handleErasing()
    {
        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if ((meshCollider == null || meshCollider.sharedMesh == null) && (hit.triangleIndex * 3 + 2 <= meshCollider.sharedMesh.triangles.Length))
                return;
        
            meshCollider.sharedMesh = DeleteTriangle(hit.triangleIndex, meshCollider.sharedMesh);
            hit.transform.GetComponent<MeshFilter>().mesh = meshCollider.sharedMesh;
        }
    }


    private void handleErasing2()
    {

        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            var origin = mouseray.GetPoint(hit.distance);
            var hitColliders = Physics.OverlapSphere(origin, mainSlider.value);
            if (hitColliders.Count()>0) {
                foreach(var collider in hitColliders)
                {

                }
                Debug.Log(hit.triangleIndex);
            }           
        }
    }

    private Mesh DeleteTriangle(int triangleIndex, Mesh currentMesh)
    {
        var triangles = currentMesh.triangles.ToList();

        triangles.RemoveAt(triangleIndex * 3);
        triangles.RemoveAt(triangleIndex * 3);
        triangles.RemoveAt(triangleIndex * 3);
        currentMesh.triangles = triangles.ToArray();
        currentMesh.RecalculateNormals();
        return currentMesh;
    }
}
